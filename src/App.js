import React,{useEffect} from "react";
import config from 'visual-config-exposer';
import Main from './Components/Main';
import "./styles.css";


export default function App() {
  useEffect(() => {
    var background=config.settings.backgroundImage
    console.log(background)
    document.body.style.backgroundImage="url("+background+")"
  
    })
return (
    <div className="main">
        <Main/>
   </div>
  );
}
